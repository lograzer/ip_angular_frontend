/** Represent an ingredient */
export interface Ingredient {
    /** Id of the ingredient */
    id: number;
   
    /** Url to get the image on internet */
    image: string;

    /** Name of the ingredient */
    name: string;
    
    /** Raw name of the ingredient picture */
    picture: string;
}

/** Describe a ingredient used in a recipe */
export interface DetailedIngredient extends Ingredient {
    /** Amount needed of this ingredient */
    amount: number;

    /** Unit wanted for this ingredient */
    unit: string;

    /** Detail about the treatment that must get the ingredient */
    original: string;
}

/**
 * Describe an API ingredient
 */
export interface IngredientApi {
    /** Id of the ingredient */
    id: number;

    /** Name of the ingredient */
    name: string;
    
    /** Picture of the ingredient */
    picture: string;
}

/**
 * Describe a returned api object
 */
export interface ApiResultIngredient {
    /** result of the api request */
    result: string;

    /** Ingredients used in the recipe and owned by the user */
    value: IngredientApi[];
}