import { Component, OnInit } from '@angular/core';
import { RecipesService } from 'src/app/services/recipes.service';
import { Ingredient } from 'src/app/models/ingredient/ingredient.module';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-select-ingredients',
  templateUrl: './select-ingredients.component.html',
  styleUrls: ['./select-ingredients.component.scss']
})
export class SelectIngredientsComponent implements OnInit {
  userIngredients: Ingredient[] = [];
  autocompleteIngredients = [];
  autocompleteSize = 3;
  listTitle = "In your fridge, there is...";

  constructor(private recipesService: RecipesService, private userService: UserService) { }

  ngOnInit() {
    // Load user's owned ingredients from our API
    this.userService.getIngredients().then((res: Ingredient[])=> { this.userIngredients = res;});
  }

  /**
   * Search ingredients with closest name 
   * @param query name we want to search
   */
  searchIngredients(query: string) {
    this.recipesService.get(query, this.autocompleteSize)
      .then(ingredients => this.autocompleteIngredients = ingredients);
  }

  /**
   * Return the element we want to display in order to describe an ingredient
   * @param ingredient ingredient we want to describe 
   */
  getOptionText(ingredient: Ingredient) {
    return ingredient ? ingredient.name : "";
  }

  /**
   * Delete an ingredient from the user's data
   * @param index index of the element to delete
   */
  onDeleteIngredient(index: number) {
    this.autocompleteIngredients = [];
    // Call our API to remove the ingredient to the user's list and splice the element from displaying element
    this.userService.removeIngredient(this.userIngredients.splice(index, 1)[0]);
    this.userIngredients = [...this.userIngredients];
  }

  /**
   * Add an ingredient to the user's data
   * @param ingredient ingredient to add
   */
  onAddIngredient(ingredient: Ingredient) {
    this.autocompleteIngredients = [];
    this.userIngredients = [...this.userIngredients, ingredient];
    // Call our API to add the ingredient to the user's list
    this.userService.addIngredient(ingredient);
  }

}
