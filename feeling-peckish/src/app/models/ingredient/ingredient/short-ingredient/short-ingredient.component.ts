import { Component, OnInit, Input } from '@angular/core';
import { Ingredient } from '../ingredients';

@Component({
  selector: 'app-short-ingredient',
  templateUrl: './short-ingredient.component.html',
  styleUrls: ['./short-ingredient.component.scss']
})
export class ShortIngredientComponent implements OnInit {
  @Input() ingredient: Ingredient;

  constructor() { }

  ngOnInit() {
  }

}
