import { Component, OnInit, Input } from '@angular/core';

import { Recipe } from '../recipe';

@Component({
  selector: 'app-list-detailed-recipes',
  templateUrl: './list-detailed-recipes.component.html',
  styleUrls: ['./list-detailed-recipes.component.scss']
})
export class ListDetailedRecipesComponent implements OnInit {

  /** List of recipes which will be displayed */
  @Input() recipes: Recipe[] = []

  /** Callback for the fav button */
  @Input() favClickCallback: (recipe: Recipe) => any;

  /** Callback for the see more button */
  @Input() seeMoreCallback: (recipe: Recipe) => any;

  step: number = 0;

  /** Manage user's step in the accordeon
   * @param newStep user's step in the accordeon
   */
  setStep(newStep: number) {
    if (newStep >= this.recipes.length) {
      this.step = this.recipes.length - 1;
    } else {
      if (newStep < 0) {
        this.step = 0;
      } else {
        this.step = newStep;
      }
    }
  }

  constructor() { }

  ngOnInit() { }

  

}
