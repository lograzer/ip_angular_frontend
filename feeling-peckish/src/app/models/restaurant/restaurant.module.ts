import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/core/shared.module';
import { ShortCityComponent } from './city/short-city/short-city.component';
import { DetailedRestaurantComponent } from './restaurant/detailed-restaurant/detailed-restaurant.component';
import { CardsModule, IconsModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    ShortCityComponent,
    DetailedRestaurantComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    CardsModule.forRoot(),
    IconsModule
  ],
  exports: [
    ShortCityComponent,
    DetailedRestaurantComponent,
    CommonModule
  ]
})
export class RestaurantModule { }

export { Restaurant, RestaurantResult } from './restaurant/restaurant';
export { City, CityResult } from './city/city';