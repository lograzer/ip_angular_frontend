import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  user: FormGroup

  constructor(private userService: UserService, private authService: AuthService) { }

  ngOnInit() {
    this.user = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      password: new FormControl('', Validators.required)
    });
  }

  onClickLogin() {
    const value = this.user.value;
    if (!value.email.trim() || !RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$").test(value.email)) {
      this.user.controls.email.setErrors({ emailFormat: "The format of the email isn't valid" });
    }
    if (this.user.valid) {
      this.userService.login({ email: value.email, password: value.password.trim(), newPassword: value.password.trim() })
        .then((response: any) => {
          this.authService.sendToken(response.token);
        }, (err) => {
          this.user.controls.email.setErrors({ genericError: err.error.message });
          this.user.controls.password.setErrors({ genericError: err.error.message });
          this.authService.logout();
        });
    }
  }
}
