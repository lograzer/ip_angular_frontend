import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecipesGenerationRoutingModule } from './recipes-generation.router';
import { RecipesGenerationComponent } from './recipes-generation.component';
import { SelectIngredientsComponent } from './select-ingredients/select-ingredients.component';
import { SharedModule } from 'src/app/core/shared.module';
import { IngredientModule } from 'src/app/models/ingredient/ingredient.module';
import { RecipeGenerationComponent } from './recipe-generation/recipe-generation.component';
import { FavoriteRecipesComponent } from './favorite-recipes/favorite-recipes.component';
import { IconsModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    RecipesGenerationComponent,
    SelectIngredientsComponent,
    RecipeGenerationComponent,
    FavoriteRecipesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IngredientModule,
    RecipesGenerationRoutingModule,
    IconsModule
  ]
  
})
export class RecipesGenerationModule { }
