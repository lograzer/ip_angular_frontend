import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-short-recipe',
  templateUrl: './short-recipe.component.html',
  styleUrls: ['./short-recipe.component.scss']
})
export class ShortRecipeComponent implements OnInit {

  @Input() recipe: Recipe

  constructor() { }

  ngOnInit() {
  }

}
