import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipes-generation',
  templateUrl: './recipes-generation.component.html',
  styleUrls: ['./recipes-generation.component.scss']
})
export class RecipesGenerationComponent implements OnInit {
  // TODO: use fr.json
  navOptions: {[key: string]: string} = {
    'My fridge': './search',
    'Generate': './generate',
    'Favorite': './favorite'
  }
  
  constructor() { }

  ngOnInit() {
  }

}
