import { DetailedIngredient, Ingredient } from '../ingredient/ingredients';

/**  Descibe a recipe */
export interface Recipe {
    /** Id of the recipe */
    id: number;

    /** Image describing the recipe */
    image: string;

    /** Title of the recipe */
    title: string;

    /** Ingredients used in the recipe and owned by the user */
    usedIngredients: DetailedIngredient[];

    /** Ingredients which are missing to cook the recipe */
    missedIngredients: DetailedIngredient[];

    /** Indicate if the reciped is concidered as a favorite one or no by the user */
    isFavorite: boolean;

    /** Id of the favorite list of the recipe */
    favoriteListId: number;
}

/**  Descibe a detailed recipe */
export interface DetailedRecipe extends Recipe {
    steps: Step[]

}

/**
 * Describe a specific step of a recipe
 */
export interface Step {
    /** Step's number */
    number: number;

    /** specific instruction */
    step: string;

    /** List of ingredients needed in the step */
    ingredients: Ingredient[];
}

/**
 * Describe an API recipe
 */
export interface RecipeApi {
    /** Id of the recipe */
    id: number;

    /** Title of the recipe */
    title: string;

    /** Owner of the recipe */
    userEmail: string;

    /** Id of the favorite list of the recipe */
    favoriteListId: number;
}


/**
 * Describe a returned api object
 */
export interface ApiResultRecipe {
    /** result of the api request */
    result: string;

    /** Recipes as favorite and owned by the user */
    value: RecipeApi[];
}

/**
 * Describe an API favorite recipe list
 */
export interface FavoriteRecipesList {
    /** Id of the recipe list */
    id: number;

    /** Title of the recipe list */
    title: string;

    /** Owner of the recipe list */
    ownerEmail: string;

    /** Array of recipes belonging to that list */
    recipes: Recipe[];
}

/**
 * Describe a returned api object
 */
export interface ApiResultRecipeList {
    /** result of the api request */
    result: string;

    /** Recipe lists owned by the user */
    value: FavoriteRecipesList[];
}