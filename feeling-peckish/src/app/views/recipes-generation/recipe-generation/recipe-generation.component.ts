import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { RecipesService } from 'src/app/services/recipes.service';
import { Ingredient, Recipe, DetailedRecipe } from 'src/app/models/ingredient/ingredient.module';
import { UserService } from 'src/app/services/user.service';
import { DetailedStepsRecipeComponent } from 'src/app/models/ingredient/recipes/detailed-steps-recipe/detailed-steps-recipe.component';


@Component({
  selector: 'app-recipe-generation',
  templateUrl: './recipe-generation.component.html',
  styleUrls: ['./recipe-generation.component.scss']
})
export class RecipeGenerationComponent implements OnInit {
  /** User's ingredients */
  ingredients?: Ingredient[];

  /** Generated recipes */
  recipes: Recipe[] = [];

  constructor(private recipesService: RecipesService, private userService: UserService, public dialog: MatDialog) {
    this.userService.userIngredients.subscribe(ingredients => this.ingredients = ingredients);
    this.userService.userRecipes.subscribe((recipes) => {
      recipes.forEach((recipe) => {
        const index =  this.recipes.findIndex((r) => r.id === recipe.id);
        if (index !== -1) { 
          this.recipes[index] = {
            ...this.recipes[index],
            isFavorite: true };
        }
      })
    });
  }

  ngOnInit() {
    this.onSearchRecipes();
  }

  /**
   * Search for recipes
   */
  async onSearchRecipes() {
    if (!this.ingredients) {
      this. ingredients = [] // TODO: Retrieve user's ingredients data with our API
     }
    this.recipes = await this.recipesService.find(this.ingredients, 10);
  }

  /** Update a favorite for the user.
   * If the recipe was concidered as favorite, the recipe will be deleted from the user's data
   * Ortherwise, it will be added to the user's data
   * @param recipe recipe we want to update
   */
  updateFavorite(recipe: Recipe) {
    // TODO: Call services
    if (!recipe.isFavorite) {
      this.userService.addRecipe(recipe);
    } else {
      this.userService.removeRecipe(recipe);
      const index: number = this.recipes.findIndex((r) => recipe.id === r.id);
      this.recipes[index].isFavorite = false;
    }
  }
  
  async seeMore(recipe: Recipe) {
    const detailedRecipe: DetailedRecipe[] = await this.recipesService.getRecipeDetailed(recipe.id);
      this.dialog.open(DetailedStepsRecipeComponent, {
        width: '80%',
        maxHeight: '90vh',
        autoFocus: false,
        data: {recipe, detailedRecipe}
      });
    }

}
