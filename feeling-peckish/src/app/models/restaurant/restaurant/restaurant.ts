
/** Represent a restaurant */
export interface Restaurant {
    /** Id of the restaurant */
    id: number;

    /** Name of the restaurant */
    name: string;

    /** Rating of the restaurant */
    user_rating: UserRating;

    /** Types of cuisine offered by the restaurant */
    cuisines: string;

    /** Url picture to show the restaurant (may be empty) */
    thumb: string;

    /** Url to the website of the restaurant */
    url: string;

    /** Address of the restaurant */
    location: Location;

    /** Timings of the restaurant */
    timings: string;

    /** Price bracket of the restaurant (1 pocket friendly - 4 costliest) */
    price_range: number;

}


/** Represent a restaurant address */
export interface Location {

    /** Street and number of the address */
    address: string;

    /** City of the address */
    city: string;

    /** Zipcode of the city */
    zipcode: number;
}


/** Represent a restaurant rate */
export interface UserRating {

    /** Average user's rate */
    aggregate_rating: string;

    /** Comment about rate (ex : Very good) */
    rating_text: string;

    /** Color of rate (ex : 5BA829) */
    rating_color: string;
}


/** Represent the API restaurant result */
export interface RestaurantResult {

    /** List of restaurants */
    restaurants: Restaurant[];

    /** Count of results */
    results_found: number;

    /** Start of results */
    results_start: number;

    /** Shown results */
    results_shown: number;
}
