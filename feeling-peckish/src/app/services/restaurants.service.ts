import { Injectable } from '@angular/core';
import { Restaurant, RestaurantResult } from '../models/restaurant/restaurant/restaurant';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { City, CityResult } from '../models/restaurant/city/city';

const API_URL: string = "https://developers.zomato.com/api/v2.1";
const API_KEY: string = "4eff4bf84d2d79bbf1923ffa6052f8b0";

const CITIES_URL: string = API_URL + "/cities";
const RESTAURANTS_URL: string = API_URL + "/search";

const PREFIX_QUERY: string = "q=";
const PREFIX_START: string = "start=";
const PREFIX_MAX_RESULT: string = "count=";
const PREFIX_CITY: string = "entity_id=";
const PREFIX_TYPE: string = "entity_type=city";
const PREFIX_SORT: string = "sort=";

const city_max_result: number = 5;
const restaurant_max_result: number = 10;
const sort: string = "rating";

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  /** Restaurants of the current search */
  restaurantsList: BehaviorSubject<Restaurant[]> = new BehaviorSubject([])

  constructor(private http: HttpClient) { }


  //************ CITIES ********************** */
  /**
 * Get an amount of cities depending on the query (autocomplete search)
 * @param query beginning of the city's name
 */
  async getCities(query: string): Promise<City[]> {
    let url = CITIES_URL + "?" + PREFIX_QUERY + query + "&" + PREFIX_MAX_RESULT + city_max_result;
    const header = new HttpHeaders({
      'user-key': API_KEY
    });
    let options = {
      headers: header
    };
    return this.http.get<CityResult>(url, options).toPromise().then(
      (response: CityResult) => {
        if (response.status == "success") {
          return response.location_suggestions;
        }
        return [];
      })
  }

  //************ RESTAURANTS****************** */
  /**
  * Get an amount of restaurants depending on the city
  * @param city choosen city
  */
  async getRestaurants(city: City, restaurant_start: number): Promise<Restaurant[]> {
    let url = RESTAURANTS_URL + "?"
      + PREFIX_CITY + city.id +
      "&" + PREFIX_TYPE +
      "&" + PREFIX_START + restaurant_start +
      "&" + PREFIX_MAX_RESULT + restaurant_max_result +
      "&" + PREFIX_SORT + sort;
    const header = new HttpHeaders({
      'user-key': API_KEY
    });
    let options = {
      headers: header
    };
    return this.http.get<RestaurantResult>(url, options).toPromise().then(
      (response: RestaurantResult) => {
        restaurant_start += restaurant_max_result;
        if (response.results_found > 0) {
          return response.restaurants;
        }
        return [];
      })
  }
}
