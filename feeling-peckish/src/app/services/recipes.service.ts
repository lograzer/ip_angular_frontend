import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Ingredient, Step } from '../models/ingredient/ingredient.module';
import { Recipe, DetailedRecipe } from '../models/ingredient/ingredient.module';

const API_URL = "https://api.spoonacular.com";

const API_INGREDIENTS_URL = API_URL + "/food/ingredients";
const API_RECIPES_URL = API_URL + "/recipes"

const API_INGREDIENTS_AUTOCOMPLETE_URL = API_INGREDIENTS_URL + "/autocomplete";
const API_RECIPES_FIND = API_RECIPES_URL + "/findByIngredients";
const API_RECIPES_INSTRUCTION = API_RECIPES_URL + "/{id}/analyzedInstructions";
const API_RECIPE_INFORMATION = API_RECIPES_URL + "/{id}/information";

// d74ce683aad84cbfbceb735605072b92
// ee67ad2692a74f768f62d34a32885417
// da7f65e8a0fa4f019b00e715f81a06b9
const API_KEY = "apiKey=ee67ad2692a74f768f62d34a32885417"
const IMAGE_PREFIX = "https://spoonacular.com/cdn/ingredients_100x100/";

const QUERY_PREFIX = "query=";
const NUMBER_ELEMENT_PREFIX = "number=";
const INGREDIENTS_PREFIX = "ingredients=";
const IGNORE_PANTRY_PREFIX = "ignorePantry=";
const RANKING_PREFIX = "ranking=";
const METAINFO_PREFIX = "metaInformation=";

@Injectable({
  providedIn: 'root'
})
export class RecipesService {  

  constructor(private http: HttpClient) { }

  /**
   * Get an amount of ingredients depending on the query (autocomplete search)
   * @param query beginning of the ingredient's name
   * @param numberElt number of element to get (maximum) 
   */
  async get(query: string, numberElt: number): Promise<Ingredient[]> {
    let request = API_INGREDIENTS_AUTOCOMPLETE_URL + "?";
    request += QUERY_PREFIX + query + "&" + NUMBER_ELEMENT_PREFIX + numberElt + "&" + METAINFO_PREFIX + "true" + "&" + API_KEY;
    return this.http.get<Ingredient[]>(request).toPromise().then(
      (ingredients: Ingredient[]) => {
        ingredients.forEach((ingredient) => { 
          ingredient.picture = ingredient.image;
          ingredient.image = IMAGE_PREFIX + ingredient.image;
         })
        return ingredients;
      })
  }

  /**
   * Find a list of recipes that we can cook withgiven ingredients
   * @param ingredients Ingredients we may want to use in recipes
   * @param numberElt Number of recipes we want to get
   * @returns Promise containing the founded recipes
   */
  async find(ingredients: Ingredient[], numberElt: number): Promise<Recipe[]> {
    if (!ingredients || ingredients.length == 0) { throw new Error('You need to indicate some ingredients if you want to cook something...') }
    const ingredientsString: string = ingredients.map((ingredient: Ingredient) => {
      return ingredient.name;
    }).join(',');
    let request = API_RECIPES_FIND + "?" + RANKING_PREFIX + "2&"+IGNORE_PANTRY_PREFIX + "true&" + NUMBER_ELEMENT_PREFIX + numberElt + "&";
    request += INGREDIENTS_PREFIX + ingredientsString + "&" + API_KEY;
    return this.http.get<Recipe[]>(request).toPromise();
  }

  /**
   * Get all the detail about a specific recipe
   * @param id id of the recipe to get
   */
  async getRecipeDetailed(id: number): Promise<DetailedRecipe[]> {
    const request = API_RECIPES_INSTRUCTION.replace("{id}", id.toString()) + '?' + API_KEY;
    return this.http.get<DetailedRecipe[]>(request).toPromise().then((recipes: DetailedRecipe[]) => {
      recipes.forEach((recipe: DetailedRecipe) => {
        recipe.steps.forEach((step: Step) => {
          step.ingredients.forEach((ingredient: Ingredient) => {
            ingredient.image = IMAGE_PREFIX + ingredient.image;
          })
        })
      });
      return recipes;
    });
  }

  /**
   * Get the recipe corresponding to the given id
   * @param id id of the recipe to get
   */
  async getRecipe(id: number): Promise<Recipe> {
    const request = API_RECIPE_INFORMATION.replace("{id}", id.toString()) + '?' + API_KEY;
    return this.http.get<Recipe>(request).toPromise();
  }


}
