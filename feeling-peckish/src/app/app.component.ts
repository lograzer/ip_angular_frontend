import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authService: AuthService) { }

  /** Check if a token is saved localy */
  tokenExists() {
    return this.authService.isLoggedIn();
  }

  /** Logout current user */
  logout() {
    this.authService.logout();
  }
}
