import { Component, OnInit } from '@angular/core';
import { Restaurant } from 'src/app/models/restaurant/restaurant/restaurant';
import { RestaurantsService } from 'src/app/services/restaurants.service';
import { City } from 'src/app/models/restaurant/city/city';

@Component({
  selector: 'app-search-restaurant',
  templateUrl: './search-restaurant.component.html',
  styleUrls: ['./search-restaurant.component.scss']
})
export class SearchRestaurantComponent implements OnInit {
  restaurants: Restaurant[] = [];
  userCity: City;
  autocompleteCities = [];
  /** Start of the restaurant list */
  restaurant_start: number = 0;
  hasMore: boolean = false;

  constructor(private restaurantService: RestaurantsService) { }

  ngOnInit() {
    // No restaurant displayed
  }

  /**
    * Search cities with closest name 
    * @param query name we want to search
    */
  searchCities(query: string) {
    this.restaurantService.getCities(query)
      .then(cities => this.autocompleteCities = cities);
  }

  /**
 * Return the element we want to display in order to describe a city
 * @param city city choosen 
 */
  getOptionText(city: City) {
    return city ? city.name : "";
  }

  /**
  * Search restaurants belonging to the selected city
  * @param city city to which restaurants belong
  */
  async onChooseCity(city: City) {
    this.restaurant_start = 0;
    this.restaurants = [];
    this.autocompleteCities = [];
    this.userCity = city;
    await this.restaurantService.getRestaurants(city, this.restaurant_start)
      .then((results: Restaurant[]) => {
        this.restaurants = results;
        if(results.length == 0 || results.length < 10){
          this.hasMore = false;
        }
        else{
          this.hasMore = true;
        }
      }
      );
    this.restaurant_start += 10;
  }

 /**
  * Load more restaurants belonging to the same city
  * @param city city to which restaurants belong
  */
 async loadMoreRestaurants() {
  await this.restaurantService.getRestaurants(this.userCity, this.restaurant_start)
    .then((results: Restaurant[]) => {
      this.restaurants = this.restaurants.concat(results);
      if(results.length == 0 || results.length < 10){
        this.hasMore = false;
      }
      else{
        this.hasMore = true;
      }
    }
    );
  this.restaurant_start += 10;
}

}
