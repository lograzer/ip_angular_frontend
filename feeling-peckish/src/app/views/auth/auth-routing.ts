import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LogInComponent } from './log-in/log-in.component';
import { AuthComponent } from './auth.component';



const routes: Routes = [
  {
    path: '', 
    component: AuthComponent,
    children: [
      { path: 'logIn', component: LogInComponent },
      { path: 'register', component: RegisterComponent },
      { path: '**', redirectTo: 'logIn' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }