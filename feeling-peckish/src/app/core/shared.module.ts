import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule,
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatFormFieldModule,
  MatListModule,
  MatAutocompleteModule,
  MatInputModule,
  MatExpansionModule, 
  MatDialogModule,
  MatSnackBarModule} from '@angular/material/';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StringReturnWhenDotsPipe } from './pipes/stringReturnWhenDots.pipe';

/** Angular Material imports */
const MATERIAL_IMPORTS = [
  MatSidenavModule,
  MatButtonModule,
  MatIconModule,
  MatToolbarModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatListModule,
  MatDialogModule,
  MatSnackBarModule
]

/** Angular Form imports */
const FORM_IMPORTS = [
  FormsModule,
  ReactiveFormsModule
]

const PIPES = [
  StringReturnWhenDotsPipe
]

@NgModule({
  declarations: [
    ... PIPES
  ],
  imports: [
    CommonModule,
    ... FORM_IMPORTS,
    ... MATERIAL_IMPORTS
  ],
  exports: [
    CommonModule,
    ... FORM_IMPORTS,
    ... MATERIAL_IMPORTS,
    ... PIPES
  ]
})
export class SharedModule { }