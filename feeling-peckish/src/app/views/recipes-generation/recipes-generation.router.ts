import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipesGenerationComponent } from './recipes-generation.component';
import { SelectIngredientsComponent } from './select-ingredients/select-ingredients.component';
import { FavoriteRecipesComponent } from './favorite-recipes/favorite-recipes.component';
import { RecipeGenerationComponent } from './recipe-generation/recipe-generation.component';


const routes: Routes = [
  {
    path: '', 
    component: RecipesGenerationComponent,
    children: [
      { path: 'search', component: SelectIngredientsComponent },
      { path: 'favorite', component: FavoriteRecipesComponent },
      { path: 'generate', component: RecipeGenerationComponent},
      { path: '**', component: SelectIngredientsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipesGenerationRoutingModule { }