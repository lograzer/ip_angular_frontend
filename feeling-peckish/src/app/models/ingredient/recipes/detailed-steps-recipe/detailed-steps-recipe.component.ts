import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';


import { DetailedRecipe, Step, Recipe } from '../recipe';

@Component({
  selector: 'app-detailed-steps-recipe',
  templateUrl: './detailed-steps-recipe.component.html',
  styleUrls: ['./detailed-steps-recipe.component.scss']
})
export class DetailedStepsRecipeComponent implements OnInit {
  recipe: Recipe;

  steps: DetailedRecipe;

  constructor(
    public dialogRef: MatDialogRef<DetailedStepsRecipeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {recipe: Recipe, detailedRecipe: DetailedRecipe[]}) {
      this.recipe = data.recipe;
      this.steps = data.detailedRecipe[0];
  }

  ngOnInit() {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }


}
