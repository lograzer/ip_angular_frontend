# Feeling Peckish Website 
- **Auteures** : Louise GRÄZER et Lou GRUNNER  
- **Lien vers le site** : Disponible à cette [adresse](https://feeling-peckish-front.herokuapp.com/auth/logIn)  

### Développé avec :
- Angular 8, Typescript, Node.JS

### Objectif
- **Proposer à un utilisateur affamé de l'aide dans le choix et la préparation de son repas. Soit en fonction des éléments présents dans son réfrigérateur, soit en fonction de sa ville avec des restaurants situés à proximité.**

### Ce site web middleware propose les fonctionnalités suivantes :
- Permettre la saisie des ingrédients disponibles dans le réfrigérateur
- Mettre à jour/ sauvegarder ces ingrédients
- Générer des recettes disponibles à l'aide de ces ingrédients
- Consulter le détails et la préparation de ces recettes
- Etablir des listes de recettes favorites propres à l'utilisateur
- Rechercher des restaurants à proximité et consulter le détail de chaque établissement
- Création d'un compte et connexion sur le site

### Appel des APIs suivantes :
- FeelingPeckish API : Mise à jour de l'utilisateur, des ingrédients et des recettes
- Spoonacular API : Génération et détail des ingrédients et des recettes
- Zomato API : Génération et détail des villes et restaurants aux environs


### Commandes utiles :
```
npm i // installation des librairies
```
```
ng serve // lancement de l'application sous 'feeling-peckish/src'
```