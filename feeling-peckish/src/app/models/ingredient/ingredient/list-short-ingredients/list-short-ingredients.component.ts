import { Component, OnInit, Input } from '@angular/core';
import { Ingredient } from '../ingredients';

@Component({
  selector: 'app-list-short-ingredients',
  templateUrl: './list-short-ingredients.component.html',
  styleUrls: ['./list-short-ingredients.component.scss']
})
export class ListShortIngredientsComponent implements OnInit {
  
  /** List of ingredients which will be displayed */
  @Input() ingredients: Ingredient[]

  /** Title which will me display on the top of the list */
  @Input() title: string;

  /** Activate the deletion if setted to true */
  @Input() deletion: boolean
  /** Callback when the user click on the delete button */
  @Input() deletionCallback: (index: number) => any

  constructor() { }

  ngOnInit() {
  }

}
