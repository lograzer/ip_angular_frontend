import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'stringReturnWhenDots'})
export class StringReturnWhenDotsPipe implements PipeTransform {
  transform(value: string): string {
    return value.replace('. ', '.\n');
  }
}