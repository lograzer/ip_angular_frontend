/** Describe an user */
export interface Profile {
    /** Email of the user */
    email: string;

    /** Passord of the user */
    password: string;

    /** New passord of the user when password changement */
    newPassword: string;
}