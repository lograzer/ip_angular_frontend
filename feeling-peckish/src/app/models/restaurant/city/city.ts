/** Represent a city */
export interface City {
    /** Id of the city */
    id: number;

    /** Name of the city */
    name: string;

    /** Flag of the country of the city */
    country_flag_url: string;
}


/** Represent the API city result */
export interface CityResult {

    /** List of suggestions cities */
    location_suggestions: City[];

    /** Result of the call (success/failed) */
    status: string;
}