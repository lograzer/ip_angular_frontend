import { Component, OnInit, Input } from '@angular/core';
import { Restaurant } from '../restaurant';

@Component({
  selector: 'app-detailed-restaurant',
  templateUrl: './detailed-restaurant.component.html',
  styleUrls: ['./detailed-restaurant.component.scss']
})
export class DetailedRestaurantComponent implements OnInit {


  @Input() restaurant: Restaurant;

  defaultPicture: string = '/assets/img/yummy.jfif';

  constructor() { }

  ngOnInit() {
  }

}
