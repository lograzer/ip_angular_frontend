import { Component, OnInit } from '@angular/core';
import { RecipesService } from 'src/app/services/recipes.service';
import { UserService } from 'src/app/services/user.service';
import { Recipe } from 'src/app/models/ingredient/ingredient.module';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-favorite-recipes',
  templateUrl: './favorite-recipes.component.html',
  styleUrls: ['./favorite-recipes.component.scss']
})
export class FavoriteRecipesComponent implements OnInit {
  recipes: Recipe[];

  constructor(private recipesService: RecipesService, private userService: UserService) {
    this.userService.userRecipes.subscribe((recipes) => {
      Promise.all(
        recipes.map(async (recipe: Recipe) => 
        {
          const apiRecipe = await this.recipesService.getRecipe(recipe.id);
          apiRecipe.isFavorite = true;
          return apiRecipe;
        })
      ).then((retrievedRecipes: Recipe[]) => this.recipes = retrievedRecipes);
    });
  }

  ngOnInit() {
  }

  /**
   * Delete a recipe from the user's favorite list
   * @param id Id of the recipe to delete
   */
  onDeleteFav(recipe: Recipe) {
    this.userService.removeRecipe(recipe);
  }

}
