import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRestaurantRoutingModule } from './search-generation.router';
import { SearchRestaurantComponent } from './search-restaurant/search-restaurant.component';
import { SharedModule } from 'src/app/core/shared.module';
import { RestaurantModule } from 'src/app/models/restaurant/restaurant.module';
import { ButtonsModule, IconsModule } from 'angular-bootstrap-md';

@NgModule({
  declarations: [
    SearchRestaurantComponent
  ],
  imports: [
    CommonModule,
    RestaurantModule,
    SearchRestaurantRoutingModule,
    SharedModule,
    ButtonsModule,
    IconsModule
  ]
})
export class SearchRestaurantModule { }
