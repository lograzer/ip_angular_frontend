import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: 'atHome', loadChildren: './views/recipes-generation/recipes-generation.module#RecipesGenerationModule',  canActivate: [AuthGuard] },
  { path: 'outside', loadChildren: './views/search-restaurant/search-restaurant.module#SearchRestaurantModule',  canActivate: [AuthGuard] },
  { path: 'auth', loadChildren: './views/auth/auth.module#AuthModule' },
  { path: '**', redirectTo: 'auth' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
