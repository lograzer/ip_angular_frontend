import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing';
import { LogInComponent } from './log-in/log-in.component';
import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../../core/shared.module';
import { InformationsComponent } from './informations/informations.component';
import { AuthComponent } from './auth.component';
import { RegisterSuccessComponent } from './register-success/register-success.component';

@NgModule({
  declarations: [
    LogInComponent,
    RegisterComponent,
    InformationsComponent,
    AuthComponent,
    RegisterSuccessComponent
  ],
  imports: [
    AuthRoutingModule,
    CommonModule,
    SharedModule
  ],
  exports: [
    LogInComponent,
    RegisterComponent
  ],
  entryComponents: [
    RegisterSuccessComponent
  ]
})
export class AuthModule { }
