import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/core/shared.module';
import { ListShortIngredientsComponent } from './ingredient/list-short-ingredients/list-short-ingredients.component';
import { ShortIngredientComponent } from './ingredient/short-ingredient/short-ingredient.component';
import { ShortRecipeComponent } from './recipes/short-recipe/short-recipe.component';
import { DetailedRecipeComponent } from './recipes/detailed-recipe/detailed-recipe.component';
import { ListDetailedRecipesComponent } from './recipes/list-detailed-recipes/list-detailed-recipes.component';
import { DetailedIngredientComponent } from './ingredient/detailed-ingredient/detailed-ingredient.component';
import { DetailedStepsRecipeComponent } from './recipes/detailed-steps-recipe/detailed-steps-recipe.component';
import { ListShortRecipesComponent } from './recipes/list-short-recipes/list-short-recipes.component';

@NgModule({
  declarations: [
    ListShortIngredientsComponent,
    ShortIngredientComponent,
    ShortRecipeComponent,
    DetailedRecipeComponent,
    ListDetailedRecipesComponent,
    DetailedIngredientComponent,
    DetailedStepsRecipeComponent,
    ListShortRecipesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ListShortIngredientsComponent,
    ShortIngredientComponent,
    ShortRecipeComponent,
    DetailedRecipeComponent,
    ListDetailedRecipesComponent,
    DetailedIngredientComponent,
    DetailedStepsRecipeComponent,
    ListShortRecipesComponent,
    CommonModule
  ],
  entryComponents: [
    DetailedStepsRecipeComponent
  ]
})
export class IngredientModule { }

export { DetailedIngredient, Ingredient } from './ingredient/ingredients'
export { DetailedRecipe, Recipe, Step } from './recipes/recipe'
