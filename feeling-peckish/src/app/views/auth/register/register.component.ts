import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

import { UserService } from 'src/app/services/user.service';
import { RegisterSuccessComponent } from '../register-success/register-success.component';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  /** Form of the user who is beeing inserted */
  newUser: FormGroup;

  constructor(private userService: UserService, private authService: AuthService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.newUser = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      password: new FormControl('', Validators.required),
      passwordConfirmation: new FormControl('', Validators.required)
    });
  }

  /**
   * On register click
   * Check if all the parameters to create a new user are correct and call the UserApi if everything is OK
   */
  onClickRegister() {
    const value = this.newUser.value;
    if (!value.email.trim() || !RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$").test(value.email)) {
      this.newUser.controls.email.setErrors({ emailFormat: "The format of the email isn't valid" });
    }
    if (!value.password.trim() || value.password.trim() !== value.passwordConfirmation.trim()) {
      this.newUser.controls.password.setErrors({ genericError: "Password aren't equivalent or are empty" });
      this.newUser.controls.passwordConfirmation.setErrors({ genericError: "Password aren't equivalent or are empty" });
    }
    if (this.newUser.valid) {
      this.userService.register({ email: value.email, password: value.password.trim(), newPassword: value.password.trim()})
        .then((response: any) => {
          this.authService.sendToken(response.token);
          this._snackBar.openFromComponent(RegisterSuccessComponent, {
            duration: 5000,
          });
        }, (err) => {
          this.newUser.controls.email.setErrors({ emailFormat: err.error.message });
          this.authService.logout();
        });
    }
  }

}
