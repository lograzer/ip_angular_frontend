import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-list-short-recipes',
  templateUrl: './list-short-recipes.component.html',
  styleUrls: ['./list-short-recipes.component.scss']
})
export class ListShortRecipesComponent implements OnInit {
  /** List of recipes which will be displayed */
  @Input() recipes: Recipe[];

  /** Callback for the fav button */
  @Input() favClickCallback: (recipe: Recipe) => any;
  
  constructor() { }

  ngOnInit() {
  }

}
