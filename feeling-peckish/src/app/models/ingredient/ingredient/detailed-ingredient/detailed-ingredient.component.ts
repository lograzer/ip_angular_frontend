import { Component, OnInit, Input } from '@angular/core';

import { DetailedIngredient } from '../ingredients';

@Component({
  selector: 'app-detailed-ingredient',
  templateUrl: './detailed-ingredient.component.html',
  styleUrls: ['./detailed-ingredient.component.scss']
})
export class DetailedIngredientComponent implements OnInit {

  @Input() ingredient: DetailedIngredient;

  constructor() { }

  ngOnInit() {
  }

}
