import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Ingredient, IngredientApi, ApiResultIngredient } from '../models/ingredient/ingredient/ingredients';
import { Recipe, ApiResultRecipe, FavoriteRecipesList, ApiResultRecipeList } from '../models/ingredient/recipes/recipe';
import { Profile } from '../models/profile/profile';
import { UtilsService } from './utils.service';

const API_URL: string = "https://feelingpeckish3.azurewebsites.net/api";
// const API_URL: string = "https://localhost:44351/api"; // only for local tests

const USER_URL: string = API_URL + "/Utilisateurs";
const INGREDIENTS_URL: string = API_URL + "/Ingredients";
const RECIPES_URL: string = API_URL + "/Recettes";

const REGISTER_URL: string = USER_URL + "/SignUp";
const LOGIN_URL: string = USER_URL + "/Login";
const PASSWORD_URL: string = USER_URL + "/UpdatePassword";

const API_INGREDIENTS_GETALL: string = INGREDIENTS_URL + "/GetIngredientsByOwner";
const API_INGREDIENTS_UPDATE: string = INGREDIENTS_URL + "/UpdateIngredientsByOwner";

const API_RECIPES_GETALL: string = RECIPES_URL + "/GetRecettesByOwner";
const API_RECIPES_POST: string = RECIPES_URL + "/PostRecette";
const API_RECIPES_DELETE: string = RECIPES_URL + "/DeleteRecette";
const API_RECIPES_UPDATE: string = RECIPES_URL + "/UpdateRecette";

const API_RECIPES_GETBYFAVLIST: string = RECIPES_URL + "/GetRecettesByListe";
const API_RECIPESLIST_GETALL: string = RECIPES_URL + "/GetRecetteListes";
const API_RECIPESLIST_POST: string = RECIPES_URL + "/PostRecetteListe";
const API_RECIPESLIST_DELETE: string = RECIPES_URL + "/DeleteRecetteListe";

const PREFIX_EMAIL: string = "email=";
const PREFIX_OLDPASSWORD: string = "oldPassword=";
const PREFIX_NEWPASSWORD: string = "newPassword=";
const PREFIX_OWNER: string = "owneremail=";
const PREFIX_RECIPE_ID: string = "recetteid=";
const PREFIX_RECIPE_LIST_ID: string = "recettelisteid=";

const IMAGE_PREFIX = "https://spoonacular.com/cdn/ingredients_100x100/";


@Injectable({
    providedIn: 'root'
})
export class UserService {

    /** Connected user */
    currentUser: Observable<Profile> = new Observable();

    /** Ingredients of the current user */
    userIngredients: BehaviorSubject<Ingredient[]> = new BehaviorSubject([])

    /** Recipes of the current user */
    userRecipes: BehaviorSubject<Recipe[]> = new BehaviorSubject([])

    constructor(private http: HttpClient, private utilsService: UtilsService) {
        this.getIngredients().then(data => this.userIngredients.next(data));
        this.getRecipes().then(data => this.userRecipes.next(data));
    }

    //************ AUTH ********************** */
    async register(profile: Profile) {
        const formData = new FormData();
        formData.append('email', profile.email);
        formData.append('password', profile.password);

        return this.http.post(REGISTER_URL, formData).toPromise();
    }

    async login(profile: Profile) {
        const formData = new FormData();
        formData.append('email', profile.email);
        formData.append('password', profile.password);

        return this.http.post(LOGIN_URL, formData).toPromise();
    }

    /**
        * Change password of current user
        * @param profile user we wan to update
        */
    async changePassword(profile: Profile) {
        // TODO : Update displayed elements

        // Update users's data using our API
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = PASSWORD_URL + "?" + PREFIX_EMAIL + owner + "&" + PREFIX_NEWPASSWORD + profile.newPassword + "&" + PREFIX_OLDPASSWORD + profile.password
                    this.http.put(url, options)
                        .toPromise()
                        .then((response: any) => { }, (err) => { });
                });
        }
    }

    //************ RECIPES ********************** */

    /** Get all user's recipes */
    async getRecipes(): Promise<Recipe[]> {
        let result: Array<Recipe> = [];
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = API_RECIPES_GETALL + "?" + PREFIX_OWNER + owner;
                    this.http.get<ApiResultRecipe>(url, options)
                        .toPromise()
                        .then((recipes: ApiResultRecipe) => {
                            recipes.value.forEach((recipe) => {
                                result.push(
                                    {
                                        id: recipe.id,
                                        title: recipe.title
                                    } as Recipe
                                );
                            });
                        }, (err) => { });
                });
        }
        return result;
    }

    /**
     * Add a recipe into the user's data
     * @param recipe recipe we want to add
     */
    async addRecipe(recipe: Recipe) {
        // Update display infos
        this.userRecipes.next([...this.userRecipes.value, recipe]);
        // Add favorite recipe using our API
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let data = {
                        "id": recipe.id,
                        "title": recipe.title,
                        "userEmail": owner
                    };
                    this.http.post(API_RECIPES_POST, data, options)
                        .toPromise()
                        .then((response: any) => { }, (err) => { });
                });
        }

    }

    /**
     * Remove a recipe from the user's data
     * @param recipe recipe we want to add
     */
    async removeRecipe(recipe: Recipe) {
        const recipes: Recipe[] = this.userRecipes.value;
        const index: number = recipes.findIndex((value) => value.title === recipe.title)
        recipes.splice(index, 1);
        this.userRecipes.next([...recipes]);
        // Remove favorite recipe using our API
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = API_RECIPES_DELETE + "?" + PREFIX_RECIPE_ID + recipe.id + "&" + PREFIX_OWNER + owner
                    this.http.delete(url, options)
                        .toPromise()
                        .then((response: any) => { }, (err) => { });
                });
        }

    }

    //************ INGREDIENTS ********************** */

    /** Get all user's ingredients */
    async getIngredients(): Promise<Ingredient[]> {
        let result: Array<Ingredient> = [];
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = API_INGREDIENTS_GETALL + "?" + PREFIX_OWNER + owner;
                    this.http.get<ApiResultIngredient>(url, options)
                        .toPromise()
                        .then((ingredients: ApiResultIngredient) => {
                            ingredients.value.forEach((ingredient) => {
                                result.push(
                                    {
                                        id: ingredient.id,
                                        name: ingredient.name,
                                        image: IMAGE_PREFIX + ingredient.picture,
                                        picture: ingredient.picture
                                    } as Ingredient
                                );
                            });
                        }, (err) => { });
                });
        }

        return result;
    }

    /**
     * Add an ingredient to the user's fridge
     * @param ingredient ingredient to add
     */
    async addIngredient(ingredient: Ingredient) {
        this.userIngredients.next([...this.userIngredients.value, ingredient]);
        // Add ingredient by calling our API with update of all list of ingredients
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let list: Array<IngredientApi> = [];
                    this.userIngredients.value
                        .forEach(
                            (ingredient) => {
                                list.push({
                                    id: ingredient.id,
                                    name: ingredient.name,
                                    picture: ingredient.picture
                                });
                            }
                        );
                    let data = {
                        "owneremail": owner,
                        "ingredients": list
                    };
                    this.http.put(API_INGREDIENTS_UPDATE, data, options)
                        .toPromise()
                        .then((response: ApiResultIngredient) => {
                        },
                            (err) => { });
                });
        }

    }

    /**
     * Delete an ingredient from the user's fridge
     * @param ingredient ingredient to delete
     */
    async removeIngredient(ingredient: Ingredient) {
        const ingredients: Ingredient[] = this.userIngredients.value;
        const index: number = ingredients.findIndex((value) => value.name === ingredient.name);
        ingredients.splice(index, 1);
        this.userIngredients.next([...ingredients]);
        // Remove ingredient by calling our API with update of all lists of ingredients
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let list: Array<IngredientApi> = [];
                    this.userIngredients.value
                        .forEach(
                            (ingredient) => {
                                list.push({
                                    id: ingredient.id,
                                    name: ingredient.name,
                                    picture: ingredient.picture
                                });
                            }
                        );
                    let data = {
                        "owneremail": owner,
                        "ingredients": list
                    };
                    this.http.put(API_INGREDIENTS_UPDATE, data, options)
                        .toPromise()
                        .then((response: ApiResultIngredient) => {
                        },
                            (err) => { });
                });
        }

    }

    //************ FAVORITE LISTS ******************* */

    /** Get all user's recipes belonging to a specific favorite list */
    async getRecipesByList(favoriteList: FavoriteRecipesList): Promise<Recipe[]> {
        let result: Array<Recipe> = [];
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = API_RECIPES_GETBYFAVLIST + "?" + PREFIX_OWNER + owner + "&" + PREFIX_RECIPE_LIST_ID + favoriteList.id;
                    this.http.get<ApiResultRecipe>(url, options)
                        .toPromise()
                        .then((recipes: ApiResultRecipe) => {
                            recipes.value.forEach((recipe) => {
                                result.push(
                                    {
                                        id: recipe.id,
                                        title: recipe.title
                                    } as Recipe
                                );
                            });
                        }, (err) => { });
                });
        }

        return result;
    }

    /** Get all user's recipe favorite lists */
    async getRecipeLists(): Promise<FavoriteRecipesList[]> {
        let result: Array<FavoriteRecipesList> = [];
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = API_RECIPESLIST_GETALL + "?" + PREFIX_OWNER + owner;
                    this.http.get<ApiResultRecipeList>(url, options)
                        .toPromise()
                        .then((recipeLists: ApiResultRecipeList) => {
                            recipeLists.value.forEach((recipeList) => {
                                result.push(recipeList);
                            });
                        }, (err) => { });
                });
        }
        return result;
    }

    /**
    * Update the recipe list to which the recipe belongs
    * @param recipe recipe we want to update
    */
    async updateRecipeList(recipe: Recipe) {
        // TODO : Update displayed elements

        // Update recipe list using our API
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let data = {
                        "id": recipe.id,
                        "title": recipe.title,
                        "userEmail": owner,
                        "myListeId": recipe.favoriteListId
                    };
                    this.http.put(API_RECIPES_UPDATE, data, options)
                        .toPromise()
                        .then((response: any) => { }, (err) => { });
                });
        }

    }

    /**
     * Add an empty recipe list
     * @param recipeList recipelist we want to add
     */
    async addRecipeList(recipeList: FavoriteRecipesList) {
        // TODO : Update display infos

        // Add empty favorite recipe list using our API
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let data = {
                        "title": recipeList.title,
                        "ownerEmail": owner
                    };
                    this.http.post(API_RECIPESLIST_POST, data, options)
                        .toPromise()
                        .then((response: any) => { }, (err) => { });
                });
        }

    }

    /**
     * Delete a recipe list with all its content from the user's data
     * @param recipeList recipe list to delete
     */
    async removeRecipeList(recipeList: FavoriteRecipesList) {
        // TODO : Delete displayed element

        // Remove recipe list by calling our API
        let token = localStorage.getItem("token");
        if (token) {
            this.utilsService.parseJwt(token)
                .then((r: any) => {
                    let owner = r.unique_name;
                    const header = new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    });
                    let options = {
                        headers: header
                    };
                    let url = API_RECIPESLIST_DELETE + "?" + PREFIX_RECIPE_LIST_ID + recipeList.id + "&" + PREFIX_OWNER + owner
                    this.http.delete(API_INGREDIENTS_UPDATE, options)
                        .toPromise()
                        .then((response: any) => {
                        },
                            (err) => { });
                });
        }

    }


}
