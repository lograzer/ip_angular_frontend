import { Component, OnInit, Input } from '@angular/core';
import { City } from '../city';

@Component({
  selector: 'app-short-city',
  templateUrl: './short-city.component.html',
  styleUrls: ['./short-city.component.scss']
})
export class ShortCityComponent implements OnInit {
  @Input() city: City;
  
  constructor() { }

  ngOnInit() {
  }

}
